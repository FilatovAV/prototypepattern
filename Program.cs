﻿using Prototype.Entities;
using Prototype.Enums;
using Prototype.Universe;
using System;
using System.Threading;

namespace Prototype
{
    class Program
    {
        static void Main(string[] args)
        {
            var bird1 = new Penguin("Jessie", "Lind", "Royal Penguin (0)", ConsoleColor.Yellow, BirdSex.Female);
            var bird2 = new Penguin("Ronald", "Parker", "Royal Penguin (1)", ConsoleColor.Blue, BirdSex.Male);

            var clone2 = bird2.Clone<BirdSoul>();

            Console.WriteLine($"\nBird 1: {bird1}");
            Console.WriteLine($"\nBird 2: {bird2}");
            Console.WriteLine($"\nClone bird 2: {clone2}");

            bird2.Mind.Thought = "Chik-Chirick-Bird";

            Console.WriteLine($"\nClone bird 2: {clone2}");
            Console.WriteLine($"-----------------");


            var cloneAsBird = bird1.Clone<Bird>();
            var cloneAsBirdSoul = bird1.Clone<BirdSoul>();

            Console.WriteLine($"\nClone as bird 1: {cloneAsBird}");
            Console.WriteLine($"\nClone as bird soul 1: {cloneAsBirdSoul}");

            Console.WriteLine(Environment.NewLine);
            Console.WriteLine($"------------------------------------------------------");
            Console.WriteLine($"--------------------ICloneable------------------------");

            var cloneICloneable = (BirdSoul)bird1.Clone();
            Console.WriteLine($"\nBird 1: {bird1}");
            Console.WriteLine($"\nBird 1 clone: {cloneICloneable}");

            bird1.Mind.Thought = "Chik-Chirick";

            Console.WriteLine($"\nBird 1: {bird1}");
            Console.WriteLine($"\nBird 1 clone: {cloneICloneable}");

            Console.ReadKey();
        }
    }
}
