﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prototype.Universe
{
    public static class Oracle
    {
        public static readonly Random Random = new Random();
        public static Guid GetGuid() => Guid.NewGuid();
    }
}
