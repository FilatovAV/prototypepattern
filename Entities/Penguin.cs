﻿using Prototype.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Prototype.Entities
{
    /// <summary> Пингвин. Реализует различные способы клонирования. </summary>
    public class Penguin : Bird, IMyCloneable, ICloneable
    {
        public string FirstName { get; private set; }
        public string LastName { get; private set; }

        public Penguin(string penguinName, string penguinLastName, string typeName, ConsoleColor color, BirdSex sex)
            : base(typeName, color, sex)
        {
            FirstName = penguinName;
            LastName = penguinLastName;
        }

        public void RenamePenguin(string firstName, string lastName)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
        }
        private Penguin(Penguin penguin) 
            : base(penguin, penguin.GetId(), penguin.BirthDate)
        {
            FirstName = penguin.FirstName;
            LastName = penguin.LastName;
            Mind.Thought = penguin.Mind.Thought;
        }

        public override string ToString()
        {
            var result = $"\nId: {this.GetId()}\n";
            result += string.Join("\n", this.GetType().GetProperties().Select(s => $"{s.Name}: {s.GetValue(this)}").ToArray());
            return result;
        }
        /// <summary> Ручное клонирование. </summary>
        public T Clone<T>() where T : BirdSoul
        {
            return new Penguin(this) as T;
        }
        /// <summary> Клонирование по интерфейсу ICloneable. </summary>
        public object Clone()
        {
            return new Penguin(this);
        }
    }
}
