﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Prototype.Entities
{
    /// <summary> Разум птицы. </summary>
    public class BirdMind
    {
        /// <summary> Мысль. </summary>
        public string Thought { get; set; }
        public override string ToString()
        {
            return string.Join("\n", this.GetType().GetProperties().Select(s => $"{s.Name}: {s.GetValue(this)}").ToArray());
        }
    }
}
