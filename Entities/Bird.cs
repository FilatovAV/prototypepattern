﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Prototype.Enums;
using Prototype.Universe;

namespace Prototype.Entities
{
    public abstract class Bird: BirdSoul
    {
        public string TypeName { get; set; }
        public ConsoleColor Color { get; }
        public BirdSex Sex { get; }

        protected Bird(string typeName, ConsoleColor color, BirdSex sex)
            : base(Oracle.GetGuid(), DateTime.UtcNow)
        {
            Sex = sex;
            TypeName = typeName;
            Color = color;
        }

        public Bird(Bird bird, Guid id, DateTime birthDate) : base(bird)
        {
            Sex = bird.Sex;
            TypeName = bird.TypeName;
            Color = bird.Color;
        }

        public override string ToString()
        {
            var result = $"\nId: {this.GetId()}\n";
            result += string.Join("\n", this.GetType().GetProperties().Select(s => $"{s.Name}: {s.GetValue(this)}").ToArray());
            return result;
        }
    }
}
