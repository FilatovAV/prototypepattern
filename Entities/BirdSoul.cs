﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Prototype.Universe;

namespace Prototype.Entities
{
    /// <summary> Душа птицы. </summary>
    public abstract class BirdSoul
    {
        /// <summary> Уникальный номер в мироздании. </summary>
        private Guid Id { get; } = Oracle.GetGuid();
        /// <summary> День рождения. </summary>
        public DateTime BirthDate { get; } = DateTime.UtcNow.AddMinutes(Oracle.Random.Next(0, 10));
        /// <summary> Разум птицы. (можно судить о работе клонирования с ссылочными типами) </summary>
        public BirdMind Mind { get; set; } = new BirdMind();

        protected BirdSoul(Guid id, DateTime birthDate)
        {
            Id = id;
            BirthDate = birthDate;
        }

        protected BirdSoul(BirdSoul soul)
        {
            Id = soul.Id;
            BirthDate = soul.BirthDate;
            Mind.Thought = soul.Mind.Thought;
        }

        public Guid GetId()
        {
            return Id;
        }
        public override string ToString()
        {
            var result = $"\nId: {this.GetId()}\n";
            result += string.Join("\n", this.GetType().GetProperties().Select(s => $"{s.Name}: {s.GetValue(this)}").ToArray());
            return result;
        }
    }
}
