﻿using System;
using System.Collections.Generic;
using System.Text;
using Prototype.Entities;

namespace Prototype
{
    interface IMyCloneable
    {
        T Clone<T>() where T : BirdSoul;
    }
}
