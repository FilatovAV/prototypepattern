﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prototype.Enums
{
    public enum BirdSex
    {
        Female,
        Male
    }
}
